const AuthController = require("./controllers/AuthController");
const ExpenseController = require("./controllers/ExpenseController");
const IncomeController = require("./controllers/IncomeController");
const TrackerController = require("./controllers/TrackerController");
const UserController = require("./controllers/UserController");

const _routes = [
  // http://localhost:8000/api
  ["", AuthController],
  // http://localhost:8000/api/users
  ["users", UserController],
  // http://localhost:8000/api/tracker
  ["tracker", TrackerController],
  // http://localhost:8000/api/income
  ["income", IncomeController],
  // http://localhost:8000/api/expense
  ["expense", ExpenseController],
];

const routes = (app) => {
  _routes.forEach((route) => {
    const [url, controller] = route;

    // http://localhost:8000/api
    app.use(`/api/${url}`, controller);
  });
};

module.exports = routes;
