// module berisi fungsi - fungsi yang berkaitan dengan query database
// untuk tabel user
const prisma = require("../helpers/database");
const bcrypt = require("bcrypt");
const Joi = require("joi");

class _user {
  // list user
  getUser = async () => {
    try {
      const list = await prisma.user.findMany();
      // console.log(list);

      return {
        status: true,
        code: 201,
        message: "Data Semua User",
        data: list,
      };
    } catch (error) {
      console.log("Get User module Error :", error);

      return {
        status: false,
        error,
      };
    }
  };
  // update user
  updateUser = async (id, body) => {
    try {
      // validasi input
      const schema = Joi.object({
        name: Joi.string(),
        email: Joi.string(),
        password: Joi.string(),
      }).options({ abortEarly: false });
      const validation = schema.validate(body);

      if (validation.error) {
        const errorDetails = validation.error.details.map(
          (detail) => detail.message
        );

        return {
          status: false,
          code: 422,
          error: errorDetails.join(", "),
        };
      }
      if (body.password) {
        body.password = bcrypt.hashSync(body.password, 10);
      }

      const update = await prisma.user.update({
        where: {
          id: id,
        },
        data: {
          name: body.name,
          email: body.email,
          password: body.password,
        },
      });

      return {
        status: true,
        code: 201,
        message: "User Berhasil Diupdate",
        data: update,
      };
    } catch (error) {
      console.log("update user module Error: ", error);

      return {
        status: false,
        error,
      };
    }
  };
  // Delete User
  delUser = async (id) => {
    try {
      const schema = Joi.number().required();

      const validation = schema.validate(id);

      if (validation.error) {
        const errorDetails = validation.error.details.map(
          (detail) => detail.message
        );
        return {
          status: false,
          code: 422,
          error: errorDetails,
        };
      }
      const deleteUser = await prisma.user.delete({
        where: { id: id },
      });

      return {
        status: true,
        code: 201,
        message: "User Berhasil Didelete",
        data: deleteUser,
      };
    } catch (error) {
      console.log("delete User module Error :", error);

      return {
        status: false,
        error,
      };
    }
  };
}
module.exports = new _user();
