const Joi = require("joi");
const prisma = require("../helpers/database");
const bcrypt = require("bcrypt");
var jwt = require("jsonwebtoken");

class _auth {
  // Login User
  login = async (body) => {
    try {
      const schema = Joi.object({
        email: Joi.string().required(),
        password: Joi.string().required(),
      });
      const validation = schema.validate(body);

      if (validation.error) {
        const errorDetails = validation.error.details.map(
          (detail) => detail.message
        );

        return {
          status: false,
          code: 422,
          error: errorDetails.join(", "),
        };
      }

      // cari user dengan email
      const user = await prisma.user.findFirst({
        where: {
          email: body.email,
        },
      });

      // kalau user tidak ada return error
      if (!user) {
        return {
          status: false,
          code: 404,
          error: "User not Found",
        };
      }

      // Kalau user  ada check password
      if (!bcrypt.compareSync(body.password, user.password)) {
        return {
          status: false,
          code: 401,
          error: "Password salah",
        };
      }

      const payload = {
        id: user.id,
        email: user.email,
      };

      const token = jwt.sign(payload, "jwt-secret-code", { expiresIn: "2h" });

      return {
        status: true,
        data: {
          token,
        },
      };
    } catch (error) {
      console.log("login auth module error :", error);

      return {
        status: false,
        error,
      };
    }
  };

  // register user
  register = async (body) => {
    try {
      // validasi input
      const schema = Joi.object({
        name: Joi.string().required(),
        email: Joi.string().required(),
        password: Joi.string().required(),
      }).options({ abortEarly: false });
      const validation = schema.validate(body);

      if (validation.error) {
        const errorDetails = validation.error.details.map(
          (detail) => detail.message
        );

        return {
          status: false,
          code: 422,
          error: errorDetails.join(", "),
        };
      }
      const password = bcrypt.hashSync(body.password, 10);
      const add = await prisma.user.create({
        data: {
          name: body.name,
          email: body.email,
          password,
        },
      });

      return {
        status: true,
        code: 201,
        message: "User Berhasil di Tambah",
        data: add,
      };
    } catch (error) {
      console.log("register auth module Error: ", error);

      return {
        status: false,
        error,
      };
    }
  };
}

module.exports = new _auth();
