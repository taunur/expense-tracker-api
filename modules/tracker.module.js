const Joi = require("joi");
const prisma = require("../helpers/database");
const bcrypt = require("bcrypt");

class _tracker {
  // getBalance
  getBalance = async (body) => {
    try {
      const uBalance = await prisma.user.findMany({
        where: {
          id: body.id,
        },
        select: {
          name: true,
          balance: true,
        },
      });

      console.log("module balance", uBalance);
      return {
        status: true,
        code: 201,
        message: "Data Balance",
        data: uBalance,
      };
    } catch (error) {
      console.log("Get Balance module Error :", error);

      return {
        status: false,
        error,
      };
    }
  };

  // update balance
  updateBalance = async (body) => {
    try {
      // validasi input
      const schema = Joi.object({
        id: Joi.number().required(),
        name: Joi.string().required(),
        email: Joi.string().required(),
        password: Joi.string().required(),
        balance: Joi.number().required(),
      }).options({ abortEarly: false });
      const validation = schema.validate(body);

      if (validation.error) {
        const errorDetails = validation.error.details.map(
          (detail) => detail.message
        );

        return {
          status: false,
          code: 422,
          error: errorDetails.join(", "),
        };
      }

      const update = await prisma.user.update({
        where: {
          id: body.id,
        },
        data: {
          name: body.name,
          email: body.email,
          balance: body.balance,
          password: body.password,
        },
        select: {
          balance: true,
        },
      });
      console.log("update", update);

      return {
        status: true,
        code: 201,
        message: "Balance Berhasil Update",
        data: update,
      };
    } catch (error) {
      console.log("Update Balance module Error :", error);

      return {
        status: false,
        error,
      };
    }
  };

  //
}

module.exports = new _tracker();
