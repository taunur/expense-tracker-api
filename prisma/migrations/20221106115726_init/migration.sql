/*
  Warnings:

  - Made the column `balance` on table `tb_user` required. This step will fail if there are existing NULL values in that column.

*/
-- AlterTable
ALTER TABLE `tb_expense` MODIFY `expense` INTEGER NOT NULL DEFAULT 0;

-- AlterTable
ALTER TABLE `tb_income` MODIFY `income` INTEGER NOT NULL DEFAULT 0;

-- AlterTable
ALTER TABLE `tb_user` MODIFY `balance` INTEGER NOT NULL DEFAULT 0;
