/*
  Warnings:

  - You are about to drop the column `expense` on the `tb_tracker` table. All the data in the column will be lost.
  - You are about to drop the column `income` on the `tb_tracker` table. All the data in the column will be lost.
  - You are about to drop the column `balance` on the `tb_user` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE `tb_tracker` DROP COLUMN `expense`,
    DROP COLUMN `income`,
    ADD COLUMN `balance` BIGINT NULL;

-- AlterTable
ALTER TABLE `tb_user` DROP COLUMN `balance`;

-- CreateTable
CREATE TABLE `tb_income` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `incomeMoney` INTEGER NOT NULL,
    `description` VARCHAR(191) NOT NULL,
    `tracker_id` INTEGER NOT NULL,
    `createdAt` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
    `updatedAt` DATETIME(3) NOT NULL,

    UNIQUE INDEX `tb_income_id_key`(`id`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `tb_expense` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `expense` INTEGER NOT NULL,
    `description` VARCHAR(191) NOT NULL,
    `tracker_id` INTEGER NOT NULL,
    `createdAt` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
    `updatedAt` DATETIME(3) NOT NULL,

    UNIQUE INDEX `tb_expense_id_key`(`id`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- AddForeignKey
ALTER TABLE `tb_income` ADD CONSTRAINT `tb_income_tracker_id_fkey` FOREIGN KEY (`tracker_id`) REFERENCES `tb_tracker`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tb_expense` ADD CONSTRAINT `tb_expense_tracker_id_fkey` FOREIGN KEY (`tracker_id`) REFERENCES `tb_tracker`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;
