-- AlterTable
ALTER TABLE `tb_expense` MODIFY `kredit` BIGINT NULL,
    MODIFY `debit` BIGINT NULL;

-- AlterTable
ALTER TABLE `tb_user` MODIFY `balance` BIGINT NULL;
