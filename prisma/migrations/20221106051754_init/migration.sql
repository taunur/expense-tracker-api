/*
  Warnings:

  - You are about to drop the column `balance` on the `tb_user` table. All the data in the column will be lost.
  - You are about to drop the `tb_expense` table. If the table is not empty, all the data it contains will be lost.

*/
-- DropForeignKey
ALTER TABLE `tb_expense` DROP FOREIGN KEY `tb_expense_user_id_fkey`;

-- AlterTable
ALTER TABLE `tb_user` DROP COLUMN `balance`;

-- DropTable
DROP TABLE `tb_expense`;

-- CreateTable
CREATE TABLE `tb_tracker` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `user_id` INTEGER NOT NULL,
    `description` TEXT NOT NULL,
    `balance` BIGINT NULL,
    `expense` BIGINT NULL,
    `income` BIGINT NULL,
    `created_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
    `updated_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- AddForeignKey
ALTER TABLE `tb_tracker` ADD CONSTRAINT `tb_tracker_user_id_fkey` FOREIGN KEY (`user_id`) REFERENCES `tb_user`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;
