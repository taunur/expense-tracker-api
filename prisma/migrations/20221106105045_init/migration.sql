/*
  Warnings:

  - You are about to drop the column `incomeMoney` on the `tb_income` table. All the data in the column will be lost.
  - Added the required column `income` to the `tb_income` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE `tb_income` DROP COLUMN `incomeMoney`,
    ADD COLUMN `income` INTEGER NOT NULL;
