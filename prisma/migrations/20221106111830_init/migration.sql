/*
  Warnings:

  - You are about to alter the column `balance` on the `tb_user` table. The data in that column could be lost. The data in that column will be cast from `BigInt` to `Int`.

*/
-- AlterTable
ALTER TABLE `tb_user` MODIFY `balance` INTEGER NULL;
