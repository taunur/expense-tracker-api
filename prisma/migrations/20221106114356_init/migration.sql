/*
  Warnings:

  - You are about to drop the column `tracker_id` on the `tb_expense` table. All the data in the column will be lost.
  - You are about to drop the column `tracker_id` on the `tb_income` table. All the data in the column will be lost.
  - Added the required column `expense_id` to the `tb_expense` table without a default value. This is not possible if the table is not empty.
  - Added the required column `income_id` to the `tb_income` table without a default value. This is not possible if the table is not empty.

*/
-- DropForeignKey
ALTER TABLE `tb_expense` DROP FOREIGN KEY `tb_expense_tracker_id_fkey`;

-- DropForeignKey
ALTER TABLE `tb_income` DROP FOREIGN KEY `tb_income_tracker_id_fkey`;

-- AlterTable
ALTER TABLE `tb_expense` DROP COLUMN `tracker_id`,
    ADD COLUMN `expense_id` INTEGER NOT NULL;

-- AlterTable
ALTER TABLE `tb_income` DROP COLUMN `tracker_id`,
    ADD COLUMN `income_id` INTEGER NOT NULL;

-- AddForeignKey
ALTER TABLE `tb_income` ADD CONSTRAINT `tb_income_income_id_fkey` FOREIGN KEY (`income_id`) REFERENCES `tb_user`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tb_expense` ADD CONSTRAINT `tb_expense_expense_id_fkey` FOREIGN KEY (`expense_id`) REFERENCES `tb_user`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;
