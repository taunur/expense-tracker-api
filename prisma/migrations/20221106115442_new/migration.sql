/*
  Warnings:

  - You are about to drop the column `expense_id` on the `tb_expense` table. All the data in the column will be lost.
  - You are about to drop the column `income_id` on the `tb_income` table. All the data in the column will be lost.
  - Added the required column `user_id` to the `tb_expense` table without a default value. This is not possible if the table is not empty.
  - Added the required column `user_id` to the `tb_income` table without a default value. This is not possible if the table is not empty.

*/
-- DropForeignKey
ALTER TABLE `tb_expense` DROP FOREIGN KEY `tb_expense_expense_id_fkey`;

-- DropForeignKey
ALTER TABLE `tb_income` DROP FOREIGN KEY `tb_income_income_id_fkey`;

-- AlterTable
ALTER TABLE `tb_expense` DROP COLUMN `expense_id`,
    ADD COLUMN `user_id` INTEGER NOT NULL;

-- AlterTable
ALTER TABLE `tb_income` DROP COLUMN `income_id`,
    ADD COLUMN `user_id` INTEGER NOT NULL;

-- AddForeignKey
ALTER TABLE `tb_income` ADD CONSTRAINT `tb_income_user_id_fkey` FOREIGN KEY (`user_id`) REFERENCES `tb_user`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tb_expense` ADD CONSTRAINT `tb_expense_user_id_fkey` FOREIGN KEY (`user_id`) REFERENCES `tb_user`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;
