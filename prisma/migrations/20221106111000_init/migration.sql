/*
  Warnings:

  - You are about to drop the `tb_tracker` table. If the table is not empty, all the data it contains will be lost.

*/
-- DropForeignKey
ALTER TABLE `tb_expense` DROP FOREIGN KEY `tb_expense_tracker_id_fkey`;

-- DropForeignKey
ALTER TABLE `tb_income` DROP FOREIGN KEY `tb_income_tracker_id_fkey`;

-- DropForeignKey
ALTER TABLE `tb_tracker` DROP FOREIGN KEY `tb_tracker_user_id_fkey`;

-- AlterTable
ALTER TABLE `tb_user` ADD COLUMN `balance` BIGINT NULL;

-- DropTable
DROP TABLE `tb_tracker`;

-- AddForeignKey
ALTER TABLE `tb_income` ADD CONSTRAINT `tb_income_tracker_id_fkey` FOREIGN KEY (`tracker_id`) REFERENCES `tb_user`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `tb_expense` ADD CONSTRAINT `tb_expense_tracker_id_fkey` FOREIGN KEY (`tracker_id`) REFERENCES `tb_user`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;
