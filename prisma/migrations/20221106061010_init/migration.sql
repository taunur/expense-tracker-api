/*
  Warnings:

  - You are about to drop the column `balance` on the `tb_tracker` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE `tb_tracker` DROP COLUMN `balance`;

-- AlterTable
ALTER TABLE `tb_user` ADD COLUMN `balance` BIGINT NULL;
