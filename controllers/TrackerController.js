// Mapping function dari module ke API
const m$tracker = require("../modules/tracker.module");
const { Router } = require("express");
const response = require("../helpers/response");
const userSession = require("../helpers/middleware");

const TrackerController = Router();

/**
 * Get Tracker
 *
 * http://localhost:8000/api/tracker
 */

TrackerController.get("/", userSession, async (req, res) => {
  console.log(req.user);
  const list = await m$tracker.getBalance({ id: req.user.id });

  // response helper
  response.sendResponse(res, list);
  console.log("get tracker api", list);
});

/**
 * Update Balance
 *
 * @param {String} balance
 * http://localhost:8000/api/tracker
 */

TrackerController.put("/", userSession, async (req, res) => {
  const updateIncome = await m$tracker.updateBalance({
    id: req.user.id,
    name: req.user.name,
    email: req.user.email,
    password: req.user.password,
    balance: req.body.balance,
  });

  // response helper
  response.sendResponse(res, updateIncome);
});

module.exports = TrackerController;
