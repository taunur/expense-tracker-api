// Mapping function dari module ke API
const m$user = require("../modules/user.module");
const { Router } = require("express");
const response = require("../helpers/response");

const UserController = Router();

/**
 * Get User
 *
 * http://localhost:8000/api/users
 */

UserController.get("/", async (req, res) => {
  const list = await m$user.getUser();

  // response helper
  response.sendResponse(res, list);
  console.log(list);
});

/**
 * Update User
 * @param {number} id
 * @param {string} name
 * @param {string} email
 * @param {string} password
 *
 * http://localhost:8000/api/users/:id
 */
UserController.put("/:id", async (req, res) => {
  // req.body berisi data yang dikirim dari client
  const update = await m$user.updateUser(Number(req.params.id), req.body);

  // response helper
  response.sendResponse(res, update);
});

/**
 * Delete User
 * @param {number} id
 *
 * http://localhost:8000/api/users/:id
 */
UserController.delete("/:id", async (req, res) => {
  // req.body berisi data yang dikirim dari client
  const deleteUser = await m$user.delUser(Number(req.params.id));

  // response helper
  response.sendResponse(res, deleteUser);
});

module.exports = UserController;
